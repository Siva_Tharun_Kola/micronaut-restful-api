package employee.department.relation.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import employee.department.relation.jpa.tables.EmployeeTable;
import lombok.Builder;
import lombok.Data;

@Entity
@Table(name = EmployeeTable.NAME)
@Data
@Builder
public class EmployeeEntity {

  @Id
  @Column(name = EmployeeTable.Column.EMP_ID, nullable = false)
  private Long employeeId;

  @Column(name = EmployeeTable.Column.EMP_NAME)
  private String employeeName;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = EmployeeTable.Column.DEPT_ID, nullable = false)
  private DepartmentEntity departmentEntity;

  @Column(name = EmployeeTable.Column.EMP_SALARY)
  private double employeeSalary;

  @Column(name = EmployeeTable.Column.EMP_MANGER_ID)
  private Long employeeManagerId;

}
