package employee.department.relation.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import employee.department.relation.jpa.tables.DepartmentTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = DepartmentTable.NAME)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentEntity {

  @Id
  @Column(name = DepartmentTable.Column.DEPARTMENT_ID)
  private Long departmentId;

  @Column(name = DepartmentTable.Column.DEPARTMENT_NAME)
  private String departmentName;

  @Column(name = DepartmentTable.Column.DEPARTMENT_HEAD)
  private Long departmentHead;

  @Column(name = DepartmentTable.Column.DEPARTMENT_OFFICE_PHONE)
  private Long departmentOfficePhone;

  @Column(name = DepartmentTable.Column.DEPARTMENT_OFFICE_PORTAL)
  private String departmentOfficePortal;

}
