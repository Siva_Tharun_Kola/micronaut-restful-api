package employee.department.relation.jpa.tables;

public interface DepartmentTable {

  String NAME = "DEPT_DET";

  interface Column {
    String DEPARTMENT_ID            = "DEPT_ID";
    String DEPARTMENT_NAME          = "DEPT_NAME";
    String DEPARTMENT_HEAD          = "DEPT_HEAD";
    String DEPARTMENT_OFFICE_PHONE  = "DEPT_OFFICE_PHONE";
    String DEPARTMENT_OFFICE_PORTAL = "DEPT_OFFICE_PORTAL";
  }

}
