package employee.department.relation.jpa.tables;

public interface EmployeeTable {
  String NAME = "EMP_DET";

  interface Column {
    String EMP_ID        = "EMP_ID";
    String EMP_NAME      = "EMP_NAME";
    String DEPT_ID       = "DEPT_ID";
    String EMP_SALARY    = "EMP_SALARY";
    String EMP_MANGER_ID = "EMP_MANGER_ID";
  }
}
