package employee.department.relation.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.repository.DepartmentRepository;

@Singleton
public class DepartmentServiceImpl implements DepartmentService {

  private final DepartmentRepository departmentRepository;

  @Inject
  public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
    this.departmentRepository = departmentRepository;
  }

  @Override
  public DepartmentEntity getDepartmentDetailsById(Long departmentId) {
    return departmentRepository.getDepartmentDetails(departmentId);
  }

}
