package employee.department.relation.service;

import employee.department.relation.jpa.entities.DepartmentEntity;

public interface DepartmentService {

  DepartmentEntity getDepartmentDetailsById(Long departmentId);
}
