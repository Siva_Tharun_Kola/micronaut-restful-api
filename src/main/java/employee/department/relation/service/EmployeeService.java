package employee.department.relation.service;

import java.util.List;

import employee.department.relation.jpa.entities.EmployeeEntity;

public interface EmployeeService {

  EmployeeEntity getEmployeeDetailsById(Long employeeId);

  List<EmployeeEntity> getEmployeeDetailsByDepartmentId(Long departmentId);
}
