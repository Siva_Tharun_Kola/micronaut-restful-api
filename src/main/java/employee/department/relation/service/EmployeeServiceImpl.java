package employee.department.relation.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import employee.department.relation.jpa.entities.EmployeeEntity;
import employee.department.relation.repository.EmployeeRepository;

@Singleton
public class EmployeeServiceImpl implements EmployeeService {
  private final EmployeeRepository employeeRepository;

  @Inject
  public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  @Override
  public EmployeeEntity getEmployeeDetailsById(Long employeeId) {
    return employeeRepository.findEmployeeById(employeeId);
  }

  @Override
  public List<EmployeeEntity> getEmployeeDetailsByDepartmentId(Long departmentId) {
    return employeeRepository.findEmployeeByDepartmentId(departmentId);
  }
}
