package employee.department.relation.repository;

import org.springframework.stereotype.Repository;

import employee.department.relation.jpa.entities.DepartmentEntity;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface DepartmentRepository extends CrudRepository<DepartmentEntity,Long> {

  @Query("FROM DepartmentEntity dept WHERE dept.departmentId = :departmentId")
  DepartmentEntity getDepartmentDetails(Long departmentId);

}
