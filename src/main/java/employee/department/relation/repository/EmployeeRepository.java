package employee.department.relation.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import employee.department.relation.jpa.entities.EmployeeEntity;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long> {

  @Query("FROM EmployeeEntity emp WHERE emp.employeeId = :employeeId")
  EmployeeEntity findEmployeeById(Long employeeId);

  @Query("FROM EmployeeEntity emp WHERE emp.departmentEntity.departmentId = :departmentId")
  List<EmployeeEntity> findEmployeeByDepartmentId(Long departmentId);
}
