package employee.department.relation.controllers;

import javax.inject.Inject;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.service.DepartmentService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/departments")
public class DepartmentController {

  private final DepartmentService departmentService;

  @Inject
  public DepartmentController(DepartmentService departmentService) {
    this.departmentService = departmentService;
  }

  @Get("/id")
  public DepartmentEntity getDepartmentById(Long id) {
    return departmentService.getDepartmentDetailsById(id);
  }
}
