package employee.department.relation.controllers;

import java.util.List;

import javax.inject.Inject;

import employee.department.relation.jpa.entities.EmployeeEntity;
import employee.department.relation.service.EmployeeService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/employees")
public class EmployeeController {

  private final EmployeeService employeeService;

  @Inject
  public EmployeeController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @Get("/{id}")
  public EmployeeEntity getEmployeeById(Long id) {
    return employeeService.getEmployeeDetailsById(id);
  }

  @Get("/department_id/{id}")
  public List<EmployeeEntity> getEmployeeEntitiesByDepartmentId(Long id) {
    return employeeService.getEmployeeDetailsByDepartmentId(id);
  }

}
