package employee.department.relation.app.repository;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.jpa.entities.EmployeeEntity;
import employee.department.relation.repository.DepartmentRepository;
import employee.department.relation.repository.EmployeeRepository;
import io.micronaut.test.annotation.MicronautTest;

@MicronautTest
public class EmployeeRepositoryTest {

  @Inject
  EmployeeRepository employeeRepository;

  @Inject
  DepartmentRepository departmentRepository;

  @BeforeEach
  public void setup() {
    departmentRepository.saveAll(Arrays.asList(DepartmentEntity.builder()
        .departmentId(1l)
        .departmentName("Finance")
        .departmentHead(12l)
        .departmentOfficePhone(3232l)
        .departmentOfficePortal("Test-1")
        .build(), DepartmentEntity.builder()
        .departmentId(2l)
        .departmentName("IT")
        .departmentHead(14l)
        .departmentOfficePhone(24123l)
        .departmentOfficePortal("Test-2")
        .build()));
    employeeRepository.saveAll(Arrays.asList(EmployeeEntity.builder()
        .employeeId(10l)
        .employeeManagerId(22l)
        .employeeName("Employee-1")
        .departmentEntity(DepartmentEntity.builder()
            .departmentId(1l)
            .departmentName("Finance")
            .departmentHead(12l)
            .departmentOfficePhone(3232l)
            .departmentOfficePortal("Test-1")
            .build())
        .employeeSalary(1000d)
        .build(), EmployeeEntity.builder()
        .employeeId(20l)
        .employeeName("Employee-2")
        .employeeManagerId(22l)
        .departmentEntity(DepartmentEntity.builder()
            .departmentId(2l)
            .departmentName("IT")
            .departmentHead(14l)
            .departmentOfficePhone(24123l)
            .departmentOfficePortal("Test-2")
            .build())
        .employeeSalary(2000d)
        .build()));
  }
  @Test
  void testFindEmployeeById() {

    EmployeeEntity employeeEntity = employeeRepository.findEmployeeById(10l);
    Assertions.assertEquals(10l,employeeEntity.getEmployeeId());
    Assertions.assertEquals(22l,employeeEntity.getEmployeeManagerId());
    Assertions.assertEquals("Employee-1",employeeEntity.getEmployeeName());
    Assertions.assertEquals(1l,employeeEntity.getDepartmentEntity().getDepartmentId());
    Assertions.assertEquals("Finance",employeeEntity.getDepartmentEntity().getDepartmentName());
    Assertions.assertEquals(12l,employeeEntity.getDepartmentEntity().getDepartmentHead());
  }

  @Test
  void testFindAllEmployeesForAParticularDepartment() {
    List<EmployeeEntity> employeeEntities = employeeRepository.findEmployeeByDepartmentId(1l);
    Assertions.assertEquals(1,employeeEntities.size());
    Assertions.assertEquals(10l,employeeEntities.get(0).getEmployeeId());
    Assertions.assertEquals(22l,employeeEntities.get(0).getEmployeeManagerId());
    Assertions.assertEquals("Employee-1",employeeEntities.get(0).getEmployeeName());
  }
}
