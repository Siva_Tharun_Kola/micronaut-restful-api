package employee.department.relation.app.repository;

import java.util.Arrays;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.repository.DepartmentRepository;
import io.micronaut.test.annotation.MicronautTest;

@MicronautTest
public class DepartmentRepositoryTest {

  @Inject
  DepartmentRepository departmentRepository;

  @BeforeEach
  public void setup() {
    departmentRepository.saveAll(Arrays.asList(DepartmentEntity.builder()
        .departmentId(1l)
        .departmentName("Finance")
        .departmentHead(12l)
        .departmentOfficePhone(3232l)
        .departmentOfficePortal("Test-1")
        .build(), DepartmentEntity.builder()
        .departmentId(2l)
        .departmentName("IT")
        .departmentHead(14l)
        .departmentOfficePhone(24123l)
        .departmentOfficePortal("Test-2")
        .build()));
  }

  @Test
  void testGetDepartmentDetails() {
    DepartmentEntity departmentEntity = departmentRepository.getDepartmentDetails(1l);
    Assertions.assertEquals(1l, departmentEntity.getDepartmentId());
    Assertions.assertEquals("Finance", departmentEntity.getDepartmentName());
    Assertions.assertEquals(12l, departmentEntity.getDepartmentHead());
  }

}
