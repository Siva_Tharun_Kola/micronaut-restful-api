package employee.department.relation.app.controllers;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.repository.DepartmentRepository;
import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;

@MicronautTest
public class DepartmentControllerTest {

  @Inject
  @Client("/departments")
  RxHttpClient client;

  @Inject
  DepartmentRepository departmentRepository;

  @BeforeAll
  static void setUpContext() {
    ApplicationContext.build().environments("test").run(EmbeddedServer.class);
  }

  @BeforeEach
  public void setup() {
    departmentRepository.saveAll(Arrays.asList(DepartmentEntity.builder()
        .departmentId(1l)
        .departmentName("Finance")
        .departmentHead(12l)
        .departmentOfficePhone(3232l)
        .departmentOfficePortal("Test-1")
        .build(), DepartmentEntity.builder()
        .departmentId(1l)
        .departmentName("IT")
        .departmentHead(14l)
        .departmentOfficePhone(24123l)
        .departmentOfficePortal("Test-2")
        .build()));
  }

  @Test
  void testGetDepartmentDetails() {
    List<DepartmentEntity> departmentEntities =
        client.retrieve(HttpRequest.GET("/12"), Argument.listOf(DepartmentEntity.class)).blockingFirst();
    Assertions.assertEquals(1, departmentEntities.size());

  }
}
