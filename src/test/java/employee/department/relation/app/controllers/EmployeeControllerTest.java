package employee.department.relation.app.controllers;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import employee.department.relation.jpa.entities.DepartmentEntity;
import employee.department.relation.jpa.entities.EmployeeEntity;
import employee.department.relation.repository.DepartmentRepository;
import employee.department.relation.repository.EmployeeRepository;
import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;

@MicronautTest
public class EmployeeControllerTest {

  @Inject
  @Client("/employees")
  RxHttpClient client;

  @Inject
  EmployeeRepository employeeRepository;

  @Inject
  DepartmentRepository departmentRepository;

  @BeforeAll
  static void setUpContext() {
    ApplicationContext.build().environments("test").run(EmbeddedServer.class);
  }

  @BeforeEach
  public void setup() {
    departmentRepository.saveAll(Arrays.asList(DepartmentEntity.builder()
        .departmentId(1l)
        .departmentName("Finance")
        .departmentHead(12l)
        .departmentOfficePhone(3232l)
        .departmentOfficePortal("Test-1")
        .build(), DepartmentEntity.builder()
        .departmentId(2l)
        .departmentName("IT")
        .departmentHead(14l)
        .departmentOfficePhone(24123l)
        .departmentOfficePortal("Test-2")
        .build()));
    employeeRepository.saveAll(Arrays.asList(EmployeeEntity.builder()
        .employeeId(10l)
        .employeeManagerId(22l)
        .employeeName("Employee-1")
        .departmentEntity(DepartmentEntity.builder()
            .departmentId(1l)
            .departmentName("Finance")
            .departmentHead(12l)
            .departmentOfficePhone(3232l)
            .departmentOfficePortal("Test-1")
            .build())
        .employeeSalary(1000d)
        .build(), EmployeeEntity.builder()
        .employeeId(20l)
        .employeeName("Employee-2")
        .employeeManagerId(22l)
        .departmentEntity(DepartmentEntity.builder()
            .departmentId(2l)
            .departmentName("IT")
            .departmentHead(14l)
            .departmentOfficePhone(24123l)
            .departmentOfficePortal("Test-2")
            .build())
        .employeeSalary(2000d)
        .build()));
  }

  @Test
  void testGetEmployeeDetails() {
    List<EmployeeEntity> employeeEntities =
        client.retrieve(HttpRequest.GET("/10"), Argument.listOf(EmployeeEntity.class)).blockingFirst();
    Assertions.assertEquals(1, employeeEntities.size());
  }

  @Test
  void testGetEmployeeDetailsForaDepartment() {
    List<EmployeeEntity> employeeEntities =
        client.retrieve(HttpRequest.GET("/department_id/1"), Argument.listOf(EmployeeEntity.class)).blockingFirst();
    Assertions.assertEquals(1, employeeEntities.size());
  }

}
